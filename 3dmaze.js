var grid = [];
var current;
var stack = [];
var cells = 20;
var size = 40;
var canvas;
var engine;
var scene;
var createScene;
var sourcePlane;
function makemaze(){
    if (current){
        current.visited = true;
        
        let next = current.checkNeighbors();
        if (next){
            next.visited = true;
            stack.push(current);
            removeWalls(current, next);
            current = next;
        } else {
            current = stack.pop();
        }
    }
    while(stack.length !== 0){
        
        if (current){
            current.visited = true;
            
            let next = current.checkNeighbors();
            if (next){
                next.visited = true;
                stack.push(current);
                removeWalls(current, next);
                current = next;
            } else {
                current = stack.pop();
            }
        }
    }
}


function initMaze(){
    for(let j = 0; j < cells; j++){
        for(let i = 0; i < cells; i++){
            let cell = new Cell(i,j);
            
            grid.push(cell);
        }
    }
    current = grid[0];
    makemaze();
    makeWalls();
    engine.isPointerLock = true;
}

function initBabylon(){
    canvas = document.getElementById('canvas');
    //canvas.width = "100%";
    //canvas.height = "100vh";
    engine = new BABYLON.Engine(canvas, true);
    createScene = function(){
        let scene = new BABYLON.Scene(engine);
        //var camera = new BABYLON
        var camera = new BABYLON.UniversalCamera("UniversalCamera", new BABYLON.Vector3(0, 1, 0), scene);
        camera.setTarget(BABYLON.Vector3.Zero());
        camera.attachControl(canvas,true);
        var light = new BABYLON.PointLight("pointLight", new BABYLON.Vector3(100, 20, 100), scene);
        //var sphere = BABYLON.MeshBuilder.CreateSphere("sphere", {diameter:2}, scene);
        //var plane = BABYLON.MeshBuilder.CreatePlane("plane", {width: 6, height: 4}, scene);
        sourcePlane = new BABYLON.Plane(0, -1, 1, 0);
        sourcePlane.normalize();
        scene.gravity = new BABYLON.Vector3(0,-9.81,0);
        camera.applyGravity = true;
        camera.ellipsoid = new BABYLON.Vector3(0.5, 1.0, 0.5);
        scene.collisionsEnabled = true;
        camera.checkCollisions = true;
        camera.speed = 0.6;
        return scene;
    };
    scene = createScene(); //Call the createScene function
    initMaze();
    engine.runRenderLoop(function () { // Register a render loop to repeatedly render the scene
        scene.render();
    });
}

function makeWalls() {
    showWorldAxis(3);
    let floorMat = new BABYLON.StandardMaterial("floorMat", scene);
    floorMat.diffuseColor = new BABYLON.Color3(0.0, 0.0, 1.0);
    let floorPlane = BABYLON.MeshBuilder.CreatePlane("floorPlane", { width: 200, height: 200, sideOrientation: BABYLON.Mesh.DOUBLESIDE }, scene);
    floorPlane.position.z = floorPlane.position.z + 95;
    floorPlane.position.x = floorPlane.position.x + 95;
    floorPlane.position.y = floorPlane.position.y - 5;
    floorPlane.material = floorMat;
    let floorRads = BABYLON.Tools.ToRadians(90);
    floorPlane.rotation.x = floorRads;
    floorPlane.checkCollisions = true;
    for (let b = 0; b < cells; b++) {
        for (let a = 0; a < cells; a++) {
            let myMaterial = new BABYLON.StandardMaterial("myMaterial" + a + "" + b, scene);
            myMaterial.diffuseColor = new BABYLON.Color3(0.5, 0.1, 0);
            let index = a + (b * cells);

            //console.log(index);
            if (grid[index].walls[0]) {


                let plane = BABYLON.MeshBuilder.CreatePlane("planetop" + a + "" + b, { width: 10, height: 10, sideOrientation: BABYLON.Mesh.DOUBLESIDE }, scene);
                plane.position = new BABYLON.Vector3(10 * b, 0, 10 * a);
                //plane.position.y = plane.position.z + 0.5;
                plane.translate(BABYLON.Axis.Z, 5, BABYLON.Space.LOCAL);
                let rads = BABYLON.Tools.ToRadians(90);
                //plane.rotation.x = rads;
                plane.checkCollisions = true;

                plane.material = myMaterial;
            }
            if (grid[index].walls[1]) {


                let plane = BABYLON.MeshBuilder.CreatePlane("planeright" + a + "" + b, { width: 10, height: 10, sideOrientation: BABYLON.Mesh.DOUBLESIDE }, scene);
                plane.position = new BABYLON.Vector3(10 * b, 0, 10 * a);
                plane.translate(BABYLON.Axis.X, 5, BABYLON.Space.LOCAL);
                let rads = BABYLON.Tools.ToRadians(90);
                plane.rotation.y = rads;
                plane.checkCollisions = true;

                plane.material = myMaterial;
            }
            if (grid[index].walls[2]) {


                let plane = BABYLON.MeshBuilder.CreatePlane("planebottom" + a + "" + b, { width: 10, height: 10, sideOrientation: BABYLON.Mesh.DOUBLESIDE }, scene);
                plane.position = new BABYLON.Vector3(10 * b, 0, 10 * a);
                plane.translate(BABYLON.Axis.Z, -5, BABYLON.Space.LOCAL);
                let rads = BABYLON.Tools.ToRadians(90);
                //plane.rotation.x = rads;
                plane.checkCollisions = true;

                plane.material = myMaterial;
            }
            if (grid[index].walls[3]) {


                let plane = BABYLON.MeshBuilder.CreatePlane("planeleft" + a + "" + b, { width: 10, height: 10, sideOrientation: BABYLON.Mesh.DOUBLESIDE }, scene);
                plane.position = new BABYLON.Vector3(10 * b, 0, 10 * a);
                plane.translate(BABYLON.Axis.X, -5, BABYLON.Space.LOCAL);
                let rads = BABYLON.Tools.ToRadians(90);
                plane.rotation.y = rads;
                plane.checkCollisions = true;

                plane.material = myMaterial;
            }


        }
    }
}


var index = function(i, j){
    if (i < 0 || j < 0 || i >= cells || j >= cells){
        return -1;
    }

    return (i + (j * cells));
}

function removeWalls(a,b){
    let x = a.i - b.i;
    if (x < 0){
        a.walls[0] = false;
        b.walls[2] = false;
        
    } else if (x == 1){
        a.walls[2] = false;
        b.walls[0] = false;
    }
    x = a.j - b.j;
    if (x < 0){
        a.walls[1] = false;
        b.walls[3] = false;
    } else if (x == 1){
        a.walls[3] = false;
        b.walls[1] = false;
    }
}

function showWorldAxis(size) {
    var makeTextPlane = function (text, color, size) {
        var dynamicTexture = new BABYLON.DynamicTexture("DynamicTexture", 50, scene, true);
        dynamicTexture.hasAlpha = true;
        dynamicTexture.drawText(text, 5, 40, "bold 36px Arial", color, "transparent", true);
        var plane = BABYLON.Mesh.CreatePlane("TextPlane", size, scene, true);
        plane.material = new BABYLON.StandardMaterial("TextPlaneMaterial", scene);
        plane.material.backFaceCulling = false;
        plane.material.specularColor = new BABYLON.Color3(0, 0, 0);
        plane.material.diffuseTexture = dynamicTexture;
        return plane;
    };
    var axisX = BABYLON.Mesh.CreateLines("axisX", [
        BABYLON.Vector3.Zero(), new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, 0.05 * size, 0),
        new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, -0.05 * size, 0)
    ], scene);
    axisX.color = new BABYLON.Color3(1, 0, 0);
    var xChar = makeTextPlane("X", "red", size / 10);
    xChar.position = new BABYLON.Vector3(0.9 * size, -0.05 * size, 0);
    var axisY = BABYLON.Mesh.CreateLines("axisY", [
        BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3(-0.05 * size, size * 0.95, 0),
        new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3(0.05 * size, size * 0.95, 0)
    ], scene);
    axisY.color = new BABYLON.Color3(0, 1, 0);
    var yChar = makeTextPlane("Y", "green", size / 10);
    yChar.position = new BABYLON.Vector3(0, 0.9 * size, -0.05 * size);
    var axisZ = BABYLON.Mesh.CreateLines("axisZ", [
        BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3(0, -0.05 * size, size * 0.95),
        new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3(0, 0.05 * size, size * 0.95)
    ], scene);
    axisZ.color = new BABYLON.Color3(0, 0, 1);
    var zChar = makeTextPlane("Z", "blue", size / 10);
    zChar.position = new BABYLON.Vector3(0, 0.05 * size, 0.9 * size);
}

window.addEventListener("DOMContentLoaded", function(){
    initBabylon();
    
});
window.addEventListener("resize", function () { // Watch for browser/canvas resize events
    engine.resize();
});