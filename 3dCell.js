function Cell(i,j){
    this.i = i;
    this.j = j;
    this.x = i * size;
    this.y = j * size;
    this.walls = [true,true,true,true];
    this.visited = false;
    
}




Cell.prototype.checkNeighbors = function(){
    let neighbors = [];
    let top = grid[index(this.i+1,this.j)];
    if (top && !top.visited){
        neighbors.push(top);
    }
    let bottom = grid[index(this.i-1,this.j)];
     if (bottom && !bottom.visited){
        neighbors.push(bottom);
    }
    let left = grid[index(this.i, this.j-1)];
     if (left && !left.visited){
        neighbors.push(left);
    }
    let right = grid[index(this.i, this.j+1)];
     if (right && !right.visited){
        neighbors.push(right);
    }
    //console.log(neighbors);
    if (neighbors.length > 0){
        let r = Math.floor(Math.random() * (neighbors.length), 10);
        
        //console.log("random: " + r + " of " + neighbors.length);
        return neighbors[r];
    } else {
        return undefined;
    }


}
